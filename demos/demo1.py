# Quick demo
#- Generate a gaussian random shear map from a simple power spectrum
#- Measure the signal with and without applying a mask
#- Compute the pseudo-Cl mixing matrix from the mask, and recover the input signal
#from the masked map
import psydocl
import numpy as np
import pylab
import healpy as hp
from mpi4py import MPI
from mpi4py.MPI import ANY_SOURCE

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

#Generate a wiggly input Cl - function wigglyCl in cltools for testing
lmax = 500

if rank==0:
    ells, c_ell = psydocl.wigglyCl(0,lmax)

    #plot the power spectrum - the l*(l+1)/2pi prefactor is conventional
    #pylab.plot(ells, ells*(ells+1)*c_ell / np.pi / 2, label='input')
    #pylab.show()

    #Let generate a gaussian random field from this power spectrum...lets imagine this is an E-mode
    #shear power spectrum. Put it in a dictionary with the key 'EE'. The possible entries for this
    #dictinary are 'TT','EE','BB','TE','EB','TB'. This is CMB polarisation notation - for galaxy surveys,
    #T could be convergence or galaxy number (over)density. Since we don't provide anything but EE, the others
    #are assumed to be zero.
    Cl_dict = {"EE":c_ell}
    #choose an nside
    nside = 512
    #and generate a GaussianRealMap object.
    shear_mapper = psydocl.GaussianRealMap(nside, ells, Cl_dict)

    #To generate a gaussian random field we call realise_map. We can set P_only=T if we just want the
    #spin-2 maps (in our case g1 and g2 maps). 
    random_shear_map = shear_mapper.realise_map(P_only=True)
    #random_shear_map is a shear_mapper.Map object, which can spin-0 or spin-2. In this case it's spin-2, and
    #random_shear_map.map1 contains g1, and random_shear_map.map2 contains g2, as numpy arrays.
    #Let's have a look 

    #We can estimate the Cls from this map, using the function maps2cl_dict from shear_mapper.py
    #This is an auto-correlation, so only need to provide one map - for a x-correlation, provide another
    #map as map2=<map>. 
    cl_est = psydocl.maps2cldict(random_shear_map,lmax=lmax)

#It looks pretty unbiased...we could get 100 realisations to check...or quicker, 
#we could bin the the Cls to cut down the noise. Define bin limits and 
#make an LBinning object
num_bins=6
lmin=100
lbinner = psydocl.LBinning(lmin=lmin,lmax=250,num_ell_bins=num_bins)

if rank==0:
    print lbinner.lmin,lbinner.lmax,lbinner.num_ell_bins

    #Use bin_cl method to get binned input and output Cls
    binned_input_cl = lbinner.bin_cl(ells, c_ell)
    binnned_output_cl = lbinner.bin_cl(ells, cl_est['EE'])
    #Now plot - use mid point of ell bins as x-axis
    #pylab.plot(lbinner.ell_bin_mids, binned_input_cl,label='input')
    #pylab.plot(lbinner.ell_bin_mids, binnned_output_cl,label='estimated')
    #pylab.legend(loc=2)
    #pylab.show()

    #Ok so yeh, we can recover a full-sky power spectrum...now let's add a mask into the mix
    #In general you can use the set_mask method of Map to mask a Map object using a numpy array
    #mask (0 is masked). Let's mask such that we observe only an eigth of the sky - get the angular
    #coordinates for a map of our nside
    theta,phi = hp.pix2ang(nside, np.arange(hp.nside2npix(nside)))
    mask = np.ones_like(theta)
    to_mask = (theta<0.) | (theta>np.pi) | (phi<0.) | (phi>np.pi)
    mask[to_mask] = 0.
    print 'f_sky:', float(mask.sum())/len(mask)

    random_shear_map.set_mask(mask)

    #Now lets estimate the Cls again from this masked map:
    cl_est_masked = psydocl.maps2cldict(random_shear_map,lmax=lmax)
else:
    mask=None
comm.Barrier()
mask=comm.bcast(mask,root=0)

#So the masked Cl is totally wrong. We need to compute a mixing matrix, using the mask
#In order to get an unbiased Cl from the masked version. The notation follows 
#Hikage et al. 2010. First calculate 
mixer = psydocl.MixingMatrixFull(lmin, lmax, mask)
mix = mixer.get_mixing_mpi(comm, TT=False,TP=False)#

if rank==0:
    #mix is a dictionary of mixing matrices - the one we want is EE_EE, this tells us how
    #to transform the masked EE Cl into the true EE Cl. First, though bin the mixing matrix
    #in the same way as we've binned the Cls:
    M_binned=mixer.bin_Ms(lbinner=lbinner, nside=nside)
    mix_binned=mixer.get_EE_EE_BB_BB_binned()
    print mix_binned
    pylab.imshow(M_binned["EE_EE"],origin='lower',interpolation='nearest')
    pylab.show()

    #Now use the binned_inversion method on the masked sky Cl, to recover unbiased, binned Cl
    recovered_masked_cl = mixer.binned_inversion(ells, cl_est_masked)
    print recovered_masked_cl

    pylab.plot(ells, ells*(ells+1)*Cl_dict['EE']/2/np.pi,label='input')
    pylab.plot(lbinner.ell_bin_mids, binned_input_cl,label='input binned')
    pylab.plot(lbinner.ell_bin_mids, binnned_output_cl,label='estimated binned')
    pylab.plot(ells, ells*(ells+1)*cl_est_masked['EE']/2/np.pi,label='estimated, masked')
    pylab.plot(lbinner.ell_bin_mids, recovered_masked_cl['EE'],label='estimated, masked, inverted')
    #pylab.xlim([lmin,lmax])
    pylab.legend(loc=2)
    pylab.show()
